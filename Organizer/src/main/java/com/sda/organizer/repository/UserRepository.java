package com.sda.organizer.repository;


import com.sda.organizer.domain.User;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by maksw on 01.10.2016.
 */
public interface UserRepository extends MongoRepository<User, String> {

    User findByUserName(String userName);
}
