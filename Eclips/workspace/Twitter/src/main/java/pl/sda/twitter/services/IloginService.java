package pl.sda.twitter.services;

public interface IloginService {

	boolean login(String login, String password);

	void logout();
}
