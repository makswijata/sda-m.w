package pl.sda.twitter.services;

import java.util.List;

import pl.sda.twitter.model.Message;
import pl.sda.twitter.services.impl.MessageService;

/*
 * Interfejs dla uslugi zwiazanych z wiadomosciami
 */

public interface IMessageservices {
	/*
	 * zapisywanie nowej wiadomosci
	 */
	void insertMessage(String message);

	/*
	 * zwracanie nowej wiadomosci
	 */
	List<Message> getMessages();

}
