package pl.sda.twitter.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pl.sda.twitter.services.IMessageservices;
import pl.sda.twitter.services.impl.MessageService;

public class MessageListServlet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {




		HttpSession session = req.getSession();
		List<String> messagesFromSession = (List<String>) session.getAttribute("MessagesList");

		if (messagesFromSession == null) {
			messagesFromSession = new ArrayList<>();

	}

		IMessageservices messageservices = new MessageService();
		req.setAttribute("messageList", messageservices.getMessages());


//	req.setAttribute("messageList", messagesFromSession);

	req.getRequestDispatcher("messageList.jsp").forward(req, resp);

	}
}
