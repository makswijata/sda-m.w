package com.sda.organizer.domain;

import java.util.Date;

/**
 * Created by maksw on 28.09.2016.
 */
public class History {

    public String description;
    public Date datePicker;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public int value;
    public Currency currency;
    public Category category;

    public int userId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return datePicker;
    }

    public void setDate(Date date) {
        this.datePicker = date;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
