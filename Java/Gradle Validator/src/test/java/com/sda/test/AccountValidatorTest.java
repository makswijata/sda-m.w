package com.sda.test;

import com.sda.example.AccountValidator;
import com.sda.example.Country;
import com.sda.example.Validator;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class AccountValidatorTest {

    @Test
    public void shouldImplementsValidatorInterface() {
        //give
        AccountValidator accountValidator = new AccountValidator("PL1234567898643200");

        //when
        boolean isValidatorInterface = accountValidator instanceof Validator;

        //then
        Assert.assertTrue(isValidatorInterface);
    }

    @Test
    public void shouldRememberAccount() {
        //given
        AccountValidator accountValidator;
        final String exampleAccountNumber = "PL 1234-5678-9864-3200";
        final String exampleAccountNumberClean = "PL1234567898643200";

        //when
        accountValidator = new AccountValidator(exampleAccountNumber);

        //then
        Assert.assertTrue("Should has correct number",
                exampleAccountNumberClean.equals(accountValidator.getAccountNumber()));
    }

    @Test
    @Ignore
    public void shouldReturnCorrectCountry() {
        //given
        AccountValidator accountValidator = new AccountValidator("PL1234567890987");

        //when
        String country = accountValidator.getCountry();

        //then
        Assert.assertEquals("should return poland", country, Country.POLAND.name);

        //String someText = "khvghgij";
       // Assert.assertTrue(Character.isAlphabetic(someText.charAt(0)));
    }

    @Test
    public void shouldCheckIbanAccount() {
        //given
        AccountValidator accountValidator = new AccountValidator("PL59223165309369178512173794");

        //when
        boolean valid = accountValidator.validate();

        //then
        Assert.assertTrue("should return true", valid);
    }

}
