package com.sda.example;


public interface Validator {

    boolean validate();
}
