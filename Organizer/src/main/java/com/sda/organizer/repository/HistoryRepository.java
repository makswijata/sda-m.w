package com.sda.organizer.repository;

import com.sda.organizer.domain.History;
import com.sda.organizer.domain.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by klolo on 26.09.16.
 */
public interface HistoryRepository extends MongoRepository<History, String> {

    List<History> findByUserId (int userId);

}
