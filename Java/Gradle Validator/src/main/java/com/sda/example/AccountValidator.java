package com.sda.example;

import com.sda.example.Country;
import com.sda.example.Validator;

import java.math.BigInteger;
import java.util.logging.*;

public class AccountValidator implements Validator {

    private static final Logger LOGGER;

    private final String accountNumber;

    private final String countryCode;

    static {
        LOGGER =
                Logger.getLogger(AccountValidator.class.getName());
        try {
            Handler consoleHandler = new ConsoleHandler();
            Handler fileHandler = new FileHandler("application.log");
            fileHandler.setFormatter(new SimpleFormatter());
            LOGGER.addHandler(consoleHandler);
            LOGGER.addHandler(fileHandler);
        } catch (Exception e) {
            LOGGER.warning("Error:" + e);
        }
    }

    public AccountValidator(final String accountNumber) {
        LOGGER.info("Input account number: " + accountNumber);

        this.accountNumber = accountNumber
                .replaceAll("-", "")
                .replaceAll(" ", "");

        LOGGER.info("Input account number: " + this.accountNumber);
        countryCode = accountNumber.substring(0, 2);
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getCountry() {
        return getCountryByCode(countryCode).name;
    }

    public int getAccountLenght() {
        return getCountryByCode(countryCode).accountNumberLenght;
    }

    public Country getCountryByCode(final String code) {
        for (Country country : Country.values()) {
            if (country.code.equals(code)) {
                return country;
            }
        }
        return null;
    }

    @Override
    public boolean validate() {
        //prawidlowa dlugosc?
        if (getAccountLenght() != accountNumber.length()) {
            return false;
        }

        String temporaryAccountNumber = accountNumber + accountNumber.substring(0, 4);
        temporaryAccountNumber = temporaryAccountNumber.substring(4);

        String resultSum = "";
        for (int i = 0; i < temporaryAccountNumber.length(); i++) {
            if (Character.isAlphabetic(temporaryAccountNumber.charAt(i))) {
                resultSum += (int) temporaryAccountNumber.charAt(i) - 55;
            } else {
                resultSum += temporaryAccountNumber.charAt(i);
            }
        }

        int result = new BigInteger(resultSum)
                .mod(new BigInteger("97"))
                .intValue();
        LOGGER.fine("Result:" + result);

        if (result != 1) {
            return false;
        }
        return true;
    }

}
