package com.company;

public class Node<T extends Number> {

    public T value;

    public Node<T> nextNode;

    public Node(T someData) {
        value = someData;
    }
}
