package com.sda.organizer.service;

import com.sda.organizer.controller.HistoryController;
import com.sda.organizer.domain.History;
import com.sda.organizer.repository.HistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;


/**
 * Created by maksw on 28.09.2016.
 */
@Service
public class HistoryService {

    @Autowired
    HistoryRepository historyRepository;

    @Autowired
    UserService userService;

    public void addHistory(History history) {
        historyRepository.save(history);

    }

   public  List<History> getUserHistory() {
       return historyRepository.findByUserId(userService.getCurrentUser().getId());
   }


    public List<History> getHistoryList() {
        return historyRepository.findAll();
    }
}
