package pl.sda.twitter.services.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.postgresql.translation.messages_bg;

import pl.sda.twitter.model.Message;
import pl.sda.twitter.services.IMessageservices;

public class MessageService implements IMessageservices {

	@Override
	public void insertMessage(String message) {

		Connection connection = null;
		/// 1 Pobra poloczenie do bazy
		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "admin");
			connection.setAutoCommit(false);
			/// tresc zapytania
			String INSERT_QUERY = "INSERT INTO messages (content) VALUES (?)";

			/// wykonanie zapytania
			try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_QUERY)) {

				preparedStatement.setString(1, message);

				preparedStatement.execute();
			}

			connection.commit();

		} catch (Exception e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {

				e1.printStackTrace();
			}
			System.out.println(e.getMessage());
		} finally {
			// 4 zwolnienei zasobow
			try {
				connection.close();
			} catch (SQLException e1) {

			}
		}

	}

	@Override
	public List<Message> getMessages() {
		List<Message> messages = new ArrayList<>();
		Connection connection = null;
		/// 1 Pobra poloczenie do bazy
		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "admin");
			connection.setAutoCommit(false);
			/// tresc zapytania

			String GET_MESSAGE_QUERY = "SELECT id, content, create_data FROM messages";
			try (PreparedStatement preparedStatement = connection.prepareStatement(GET_MESSAGE_QUERY)) {
				ResultSet rs = preparedStatement.executeQuery();

				while(rs.next()) {
					Message message = new Message();
					message.setId(rs.getInt(1));
					message.setContent(rs.getString(2));
					message.setCreate_data(rs.getString(3));
					messages.add(message);
				}
			}


		} catch (Exception e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {

				e1.printStackTrace();
			}
			System.out.println(e.getMessage());
		} finally {
			// 4 zwolnienei zasobow
			try {
				connection.close();
			} catch (SQLException e1) {

			}
		}
			/// sortowanie wiadomosci
		messages.sort(new Comparator<Message>() {
        	@Override
        	public int compare(Message o1, Message o2) {
        		return o2.getCreate_data().compareTo(o1.getCreate_data());
        	}
		});

		return messages;
	}

}
