package com.sda.organizer.apllication;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * Konfiguracja springa dla aplikacji.
 */
@Configuration
@ComponentScan(basePackages = "com.sda.organizer")
@EnableMongoRepositories(basePackages = "com.sda.organizer")
class SpringApplicationConfig {

}
