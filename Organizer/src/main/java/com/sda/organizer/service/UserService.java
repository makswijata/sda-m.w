package com.sda.organizer.service;

import com.sda.organizer.domain.User;
import com.sda.organizer.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by maksw on 28.09.2016.
 */
@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    User currentUser;

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public UserRepository getUserRepository() {
        return userRepository;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void addUser(User user) {
        user.setId(userRepository.findAll().size());
        userRepository.save(user);
    }

}
