package com.sda.organizer.domain;

/**
 * Created by RENT on 2016-09-29.
 */
public enum Currency {
    PLN,
    EUR,
    GBP
}
