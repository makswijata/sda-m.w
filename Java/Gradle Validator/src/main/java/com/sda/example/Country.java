package com.sda.example;

/**
 * Enum reprezentujacy kraje w procesie
 * walidacji rachunku bankowego <b>IBAN</b>.
 * Przechowywane dane:
 * <ul>
 * <li>kod kraju</li>
 * <li>nazwa kraju</li>
 * <li>prawidlowa dlugosc numeru rachunku</li>
 * </ul>
 *
 * @author ja
 * @since 0.0.1
 */
public enum Country {

    POLAND("PL", "Polska", 28),
    UK("GB", "Wielka Brytania", 22),
    GERMANY("DE", "Niemcy", 22);

    /**
     * Minimalna dlugosc rachunku IBAN.
     */
    public static final int MIN_LENGHT = 15;

    /**
     * Kod kraju np. PL
     */
    public String code;

    /**
     * Czytelna nazwa kraju np. Wielka Brytania.
     */
    public String name;

    /**
     * Dlugosc numeru rachunku w danym kraju.
     */
    public int accountNumberLenght;

    /**
     * Inicjalizacja wszystkich danych enum-a.
     *
     * @see #code
     * @see #name
     * @see #accountNumberLenght
     */
    Country(String code, String name, int accountNumberLenght) {
        this.code = code;
        this.name = name;
        this.accountNumberLenght = accountNumberLenght;
    }

    @Override
    public String toString() {
        return name;
    }
}
