<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ include file="/WEB-INF/jsp/common/header.jsp"%>

</head>
<body>
	<%@ include file="/WEB-INF/jsp/common/navigation.jsp"%>

	<c:if test='${"SUCCESS" == requestScope.result}'>
		<span class="label label-success">Wiadomośc dodana</span>
	</c:if>

	<c:if test='${"ERROR" == requestScope.result}'>
		<span class="label label-error">Wiadomośc NIE ZOSTAŁA dodana</span>
	</c:if>

	<form action="addMessage" method="post" role="form">
		<div class="form-group">
			<label for="message">Message</label> <input name="message"
				id="message" type="text" class="form-control">
		</div>

		<button type "submit" class="btn btn-default">Add message</button>
	</form>

	<%@ include file="/WEB-INF/jsp/common/footer.jsp"%>

</body>
</html>