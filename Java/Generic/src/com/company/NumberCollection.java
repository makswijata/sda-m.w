package com.company;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;


/**
 * Created by RENT on 2016-09-19.
 */
public class NumberCollection<T extends Number> implements Collection<T> {


    private Object[] data = {};

    @Override
    public int size() {

        return data.length;
    }

    @Override
    public boolean isEmpty() {
        if (size() > 0){

            return false;
        }
        return true;
    }

    @Override
    public boolean contains(Object o) {
        for (int i= 0; i < data.length; ++i) {
            if (o == data[i]){
                return true;
            }
        }

        return false;
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }


    @Override
    public boolean remove(Object o) {
        for (int i= 0; i < data.length; ++i) {
            if (o == data[i]){
                remove(i);
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

//    @Override
//    public boolean addAll(Collection c) {
//        Object temp = data;
//        for (Object object: c){
//            add(c);
//            return true;
//
//        }
//        return false;
//    }

    @Override
    public void clear() {

    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        for(Object object: c){
            for (int i= 0; i < data.length; ++i) {
                if (c == data[i]){
                    continue;
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public Object[] toArray(Object[] a) {
        return data;
    }

    public boolean add(T o) {
        Object[] newdata = new Object[size() + 1];
        for (int i = 0; i < size(); ++i) {
            newdata[i] = data[i];
        }
        newdata[newdata.length - 1] = o;
        data = newdata;


        return true;
    }

    public boolean remove(int ind) {
        if(data.length < ind) {
            throw new IndexOutOfBoundsException("Incorrect index");
        }
        Object[] newData = new Object[size() - 1];
        for (int i = 0; i < size(); ++i) {
            if (i == ind) {
                continue;
            }
            newData[i] = data[i];
        }
        return true;
    }

    public boolean removeAll(Collection c) {
        data = new Object[0];
        return true;
    }


    public void loadFromFile (String fileName) {
            String result = "";
            File inputData = new File("data.txt");

            try(BufferedReader br = new BufferedReader(new FileReader(inputData))) {
                String line;
                while ((line = br.readLine()) != null) {
                    result += line + "\n";
                    for (int i = 0; i < line.length(); i++){
                        char currentChar = line.charAt(i);
                        if (Character.isDigit(i)){
                            continue;
                        } else {
                            throw new WrongLineEx();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
}