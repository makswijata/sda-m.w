package com.sda.organizer.controller;

import com.sda.organizer.domain.History;
import com.sda.organizer.repository.HistoryRepository;
import com.sda.organizer.service.HistoryService;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by RENT on 2016-09-29.
 */
@Component
public class HistoryController extends BaseController {
    @FXML
    public TableView historyTable;
    public TableColumn date;
    public Button addMore;

    public void onShowHistory(ActionEvent actionEvent) {
    }

    @Autowired
    HistoryService historyService;

    @Autowired
    HistoryRepository historyRepository;

    @FXML
    public void initialize() {
        historyTable.setItems(FXCollections.observableArrayList(historyService.getUserHistory()));
    }

    public void onAddMore(ActionEvent actionEvent) {
        switchStage(addMore, "/scenes/add/add.fxml");
    }
}
