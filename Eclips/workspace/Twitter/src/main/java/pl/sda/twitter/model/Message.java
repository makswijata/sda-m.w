package pl.sda.twitter.model;

/*
 * reprezentuje wiadomo� w aplikacji
 */

public class Message {
	
	private String content;
	
	private int id;
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCreate_data() {
		return create_data;
	}

	public void setCreate_data(String create_data) {
		this.create_data = create_data;
	}

	private String create_data;
}
