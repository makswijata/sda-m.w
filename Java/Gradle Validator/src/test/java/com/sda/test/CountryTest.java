package com.sda.test;

import com.sda.example.Country;
import org.junit.Assert;
import org.junit.Test;

public class CountryTest {

    @Test
    public void shouldHasCorrectToString() {
        //given
        Country poland = Country.POLAND;

        //when
        String toStringResult = poland.toString();

        //then
        Assert.assertTrue("Should return name of country", Country.POLAND.name.equals(toStringResult));
    }

    @Test
    public void shouldHasNotEmptyField() {
        //given
        Country[] countries = Country.values();

        for (Country country : countries) {
            checkField(country.code);
            checkField(country.name);

            Assert.assertNotNull("Should accountNumberLenght be correct",
                    country.accountNumberLenght > Country.MIN_LENGHT);
        }
    }

    private void checkField(String field) {
        Assert.assertNotNull("Should " + field + " not be null", field);
        Assert.assertNotEquals("Should " + field + " not be null", "", field);
        Assert.assertTrue("Should " + field + " be longer then 3", field.length() >=2);
    }

}
