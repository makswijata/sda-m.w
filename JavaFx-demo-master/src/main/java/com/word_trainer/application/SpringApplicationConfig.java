package com.word_trainer.application;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Konfiguracja springa dla aplikacji.
 */
@Configuration
@ComponentScan(basePackages = "com.word_trainer")
class SpringApplicationConfig {

}
