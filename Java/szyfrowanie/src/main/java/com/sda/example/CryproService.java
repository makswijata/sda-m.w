package com.sda.example;

/**
 * Created by RENT on 2016-09-14.
 */
public interface CryproService {

    String decrypt(String input, String password);

    String encrypt(String input, String password);
}
