package com.sda.organizer.domain;

/**
 * Created by maksw on 01.10.2016.
 */
public enum Category {

    DZIWKI,
    JEDZENIE,
    MIESZKANIE,
    UZYWKI,
    SAMOCHOD,
    INNE
}
