package com.sda.organizer.domain;

import org.springframework.data.annotation.Id;

/**
 * Created by maksw on 28.09.2016.
 */
public class User {
    @Id
    public int id;

    public String userName;
    public String userPassword;
    public String email;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
