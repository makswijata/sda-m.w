package com.sda.example;


/**
 * Created by RENT on 2016-09-14.
 */
public class SimpleCryptoService implements CryproService {

    private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(CryproService.class);
    private TableService tableService = new TableService();


    @Override
    public String encrypt(String input, String password) {
        LOGGER.info("decrypt data: {}", input);
        final String fillText = tableService.fillToBlockSize(input);
        final String[][][] stringBlock = tableService.splitToBlock(fillText);
        final Integer [][][] asciTextBlock = tableService.convertToAsci(stringBlock);
        for (int i = 0; i < stringBlock.length; ++i) {
            tableService.show(stringBlock[i]);
        }
        for (int i = 0; i < asciTextBlock.length; ++i) {
            tableService.show(asciTextBlock[i]);
        }
        final String[][] passwordBlock = tableService.toSingleBlock(password);
        tableService.show(passwordBlock);

        Integer [][] asciPasswordBlock =tableService.convertToAsci(passwordBlock);
        Integer [][][] codedResult = new Integer[asciTextBlock.length][asciPasswordBlock[0].length][asciPasswordBlock[0].length];


        for (int i = 0; i < asciTextBlock.length; ++i) {
            codedResult[i] = tableService.add(asciTextBlock[i], asciPasswordBlock);
            tableService.show(codedResult[i]);
            codedResult[i] =tableService.shift(codedResult[i]);
            tableService.show(codedResult[i]);
        }


        return "Tekst decrypt";
    }

    @Override
    public String decrypt(final String input, final String password) {
        LOGGER.info("Input string: {}", input);

        final String[][] passwordBlock = tableService.toSingleBlock(password);
        Integer[][] asciiPasswordBlock = tableService.convertToAsci(passwordBlock);

        String result = "";
        final String[][][] stringBlocks = tableService.splitToBlock(input);
        for (int i = 0; i < stringBlocks.length; ++i) {
            Integer[][] intBlock = tableService.convertToAsci(stringBlocks[i]);
            intBlock = tableService.shift(intBlock);
            result += tableService.convertFromBlock(tableService.sub(intBlock, asciiPasswordBlock));
        }

        return result;
    }
}
