package com.sda.iManu.domain;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Represents customer data.
 */
@Setter
@Getter
@ToString
public class User {

    @Id
    private String id;

    @NotNull
    @Min(2)
    @Max(20)
    private String firstName;

    @NotNull
    @Min(2)
    @Max(20)
    private String lastName;

    @NotNull
    @Min(5)
    @Max(12)
    @Indexed(unique = true)
    private String login;

    @NotNull
    @Min(5)
    @Max(12)
    private String password;

    public User(final String firstName, final String lastName, final String login, final String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.login = login;
        this.password = password;
    }
}
