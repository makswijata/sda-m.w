package com.sda.example;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Podstawowy kontroller dla widoku JavyFx.
 */
public class ApplicationController {

    @FXML
    private TextArea in;

    @FXML
    private TextArea out;

    @FXML
    private TextField passField;

    @FXML
    private CheckBox checkBox;

    private SimpleCryptoService simpleCryptoService = new SimpleCryptoService();
    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationController.class);

    public void onRun(ActionEvent actionEvent) {
        LOGGER.info("start");
        if (checkBox.isSelected()) {
            simpleCryptoService.decrypt(in.getText(), passField.getText());
        } else {
            simpleCryptoService.encrypt(in.getText(), passField.getText());
        }
    }

    public void onOpen() {
        LOGGER.info("Open");
    }

    public void status() {
        SimpleCryptoService simpleCryptoService = new SimpleCryptoService();
        String temp;
        if (checkBox.isSelected()) {
            temp = simpleCryptoService.decrypt(in.getText(), passField.getText());

        } else {
            temp = simpleCryptoService.encrypt(in.getText(), passField.getText());

        }
        out.setText(temp);
    }
}
