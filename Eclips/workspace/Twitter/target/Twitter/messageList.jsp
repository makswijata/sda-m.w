<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ include file="/WEB-INF/jsp/common/header.jsp"%>


<script type="text/javascript">
	function removeMessage(id) {
		$('#removeMsgForm > input[name="messageId"]}').val(id);
		$('#removeMsgForm').submit();
	}
</script>


</head>
<body>
	<%@ include file="/WEB-INF/jsp/common/navigation.jsp"%>
	<table class="table">
		<thead>
			<tr>
				<td>Id</td>
				<td>Content</td>
				<td>Create date</td>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="message" items="${requestScope.messageList }">
				<tr>

					<td><c:out value="${message.id }" /></td>
					<td><c:out value="${message.content }" /></td>
					<td><c:out value="${message.create_data }" /></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>



	<%@ include file="/WEB-INF/jsp/common/footer.jsp"%>

</body>
</html>