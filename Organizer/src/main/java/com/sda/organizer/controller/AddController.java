package com.sda.organizer.controller;

import com.sda.organizer.apllication.SpringFXLoader;
import com.sda.organizer.domain.Category;
import com.sda.organizer.domain.Currency;
import com.sda.organizer.domain.History;
import com.sda.organizer.domain.User;
import com.sda.organizer.service.HistoryService;
import com.sda.organizer.service.UserService;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by maksw on 28.09.2016.
 */
@Component
public class AddController extends BaseController {
    @FXML
    public ChoiceBox currencyChoice;
    @FXML
    public TextField description;
    @FXML
    public Button OK;
    @FXML
    public DatePicker datePicker;
    @FXML
    public TextField value;
    @FXML
    public ChoiceBox categoryChoice;


    @Autowired
    HistoryService historyService;

    @Autowired
    UserService userService;




    public void onAdd(ActionEvent actionEvent) {
        History history = new History();
        history.setDescription(description.getText());
        history.setValue(Integer.parseInt(value.getText()));
        Date tmpDate = Date.from(datePicker.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());
        history.setDate(tmpDate);
        history.setCurrency((Currency) currencyChoice.getSelectionModel().getSelectedItem());
        history.setUserId(userService.getCurrentUser().getId());

        String selectedCategory = (String) categoryChoice.getSelectionModel().getSelectedItem();
        for (Category category : Category.values()){
            String label = SpringFXLoader.resourceBundle.getString("category."+category);
            if(label.equals(selectedCategory)) {
                history.setCategory(category);
                break;
            }
        }

        historyService.addHistory(history);

        switchStage(currencyChoice, "/scenes/history/history.fxml");
    }

    @FXML
    public void initialize() {
        List<Currency> currencyList = new LinkedList<>();
        for(Currency current : Currency.values()){
            currencyList.add(current);
        }
        List<String> categoryList = new LinkedList<>();
        for (Category category : Category.values()){
            String label = SpringFXLoader.resourceBundle.getString("category."+category);
            categoryList.add(label);
        }

        currencyChoice.setItems(FXCollections.observableArrayList(currencyList));
        categoryChoice.setItems(FXCollections.observableArrayList(categoryList));

    }
}
