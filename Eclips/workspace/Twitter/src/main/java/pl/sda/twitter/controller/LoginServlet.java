package pl.sda.twitter.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.sda.twitter.model.SessionData;
import pl.sda.twitter.services.IloginService;
import pl.sda.twitter.services.impl.LoginService;

public class LoginServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


		if(!"".equals(req.getParameter("logout"))) {
			req.getSession().invalidate();
		}


		req.getRequestDispatcher("login.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String login = req.getParameter("login");
		String password = req.getParameter("inputPassword");
		/// sprawdzenie porpawnosci logowania

		IloginService loIloginService = new LoginService();

		if (loIloginService.login(login, password)) {

			SessionData sessionData = new SessionData();
			sessionData.setLogin(login);
			req.getSession().setAttribute("SessionData", sessionData);

			resp.sendRedirect(req.getContextPath() + "/messageList");
		} else {
			req.setAttribute("Status", "ERROR");
			req.getRequestDispatcher("login.jsp").forward(req, resp);
		}

		// /// poprawny login
		// req.getRequestDispatcher(req.getContextPath() +
		// "/messageList").forward(req, resp);
		// /// bledny login
		// req.setAttribute("Status", "ERROR");
		// req.getRequestDispatcher("login.jsp").forward(req, resp);
	}

}
