package com.sda.organizer.controller;

import com.sda.organizer.domain.User;
import com.sda.organizer.service.UserService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.security.*;



/**
 * Created by maksw on 27.09.2016.
 */
@Component
public class RegistryController extends BaseController {

    @FXML
    public Label label1;

    @FXML
    public TextField loginArea;
    @FXML
    public PasswordField password;
    @FXML
    public TextField email;

    @Autowired
    UserService userService;


    public void onRegistry(ActionEvent actionEvent) {
        User user = new User();
        byte [] bytesOfPassword = password.getText().getBytes();
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] thedigest = md.digest(bytesOfPassword);
            String hashPassword = new String(thedigest);
            user.setUserPassword(hashPassword);


        } catch (Exception e) {
            e.printStackTrace();
        }

        user.setUserName(loginArea.getText());
        user.setEmail(email.getText());
        userService.addUser(user);

        switchStage(label1,"/scenes/add/add.fxml");
    }

}
