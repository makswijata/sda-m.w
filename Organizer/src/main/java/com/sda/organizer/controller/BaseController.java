package com.sda.organizer.controller;

import com.sda.organizer.apllication.SpringFXLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


/**
 * Base class for all constroller
 */
public abstract class BaseController {
        protected SpringFXLoader springFXLoader = new SpringFXLoader();

    protected void switchStage(final Node node, final String stageToLoad)  {
        final Stage rootStage = (Stage) node.getScene().getWindow();
        Scene scene = new Scene((Parent) SpringFXLoader.load(stageToLoad), 600, 600);
        rootStage.setScene(scene);
    }
}
