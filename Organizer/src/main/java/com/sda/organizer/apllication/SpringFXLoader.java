package com.sda.organizer.apllication;

import javafx.fxml.FXMLLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by RENT on 2016-09-20.
 */
@Configuration
@ComponentScan(basePackages = "com.sda.organizer")
public class SpringFXLoader {

    private static final String BUNDLES_PATH = "common//bundles//messages";

    private static String currentLocaleLang = "pl";

    private static final Logger log = LoggerFactory.getLogger(SpringFXLoader.class);

    private static final ApplicationContext applicationContext
            = new AnnotationConfigApplicationContext(SpringFXLoader.class);

    public static ResourceBundle resourceBundle;

    public static Object load(final String url) {
        log.info("Load stage: {}", url);

        try (final InputStream fxmlStream = SpringFXLoader.class.getResourceAsStream(url)) {
            final FXMLLoader loader = new FXMLLoader();
            resourceBundle = ResourceBundle.getBundle(BUNDLES_PATH, new Locale(currentLocaleLang));
            loader.setResources(resourceBundle);
            loader.setControllerFactory(applicationContext::getBean);
            loader.setLocation(Class.class.getResource(url));
            return loader.load(fxmlStream);
        }
        catch (final IOException ioException) {
            throw new RuntimeException(ioException);
        }
    }

    public static String getCurrentLocaleLang() {
        return currentLocaleLang;
    }

    public static void setCurrentLocaleLang(String currentLocaleLang) {
        SpringFXLoader.currentLocaleLang = currentLocaleLang;
    }
}
