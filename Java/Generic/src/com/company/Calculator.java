package com.company;

/**
 * Created by RENT on 2016-09-17.
 */
public class Calculator <T extends Number>{
    public int add (T... input) {
        int resoult = 0;
        for (T tem : input){
            resoult += tem.intValue();
        }
        return resoult;
    }

    public int multiply (T... input) {
        int resoult = 0;
        for (T tem : input){
            resoult *= tem.intValue();
        }
        return resoult;
    }
}
