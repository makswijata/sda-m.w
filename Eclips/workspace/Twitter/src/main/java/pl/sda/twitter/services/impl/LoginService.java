package pl.sda.twitter.services.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import pl.sda.twitter.services.IloginService;

public class LoginService implements IloginService {

	@Override
	public boolean login(String login, String password) {
		Connection connection = null;
		/// 1 Pobra poloczenie do bazy
		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "admin");
			connection.setAutoCommit(false);
			/// tresc zapytania
			String GET_LOGIN_QUERY = "SELECT * FROM \"twitterUsers\" WHERE login = ? AND password = ?";

			/// wykonanie zapytania

			try (PreparedStatement preparedStatement = connection.prepareStatement(GET_LOGIN_QUERY)) {

				preparedStatement.setString(1, login);
				preparedStatement.setString(2, password);

				ResultSet rs = preparedStatement.executeQuery();

				if (rs.next()) {
					return true;
				}

			}

		} catch (Exception e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {

				e1.printStackTrace();
			}
			System.out.println(e.getMessage());
		} finally {
			// 4 zwolnienei zasobow
			try {
				connection.close();
			} catch (SQLException e1) {

			}
		}
		return false;

	}

	@Override
	public void logout() {

	}

}
