package com.sda.example;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Podstawowy kontroller dla widoku JavyFx.
 */
public class ApplicationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationController.class);

    @FXML
    private TextField inputAccountNumber;

    @FXML
    private Label validation;

    public void onValidate() {

        LOGGER.info("input data: {}", inputAccountNumber.getText());
        AccountValidator accountValidator = new AccountValidator(inputAccountNumber.getText());
        if (accountValidator.validate()) {
            validation.setStyle("-fx-background-color:limegreen");
            validation.setText("Numer poprawny");

        } else {
            validation.setStyle("-fx-background-color:tomato");
            validation.setText("Numer bledny");
        }
    }

}
