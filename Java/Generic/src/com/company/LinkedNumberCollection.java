package com.company;

/**
 *
 */
public class LinkedNumberCollection<T extends Number> {

    private Node<T> firstElement;


    public int size() {
        int temp = 0;

        if (firstElement == null) {
            return temp;
        } else {
            Node nextElement = firstElement.nextNode;
            while (nextElement != null) {
                temp += 1;
                nextElement = nextElement.nextNode;
            }
        }
        return temp;
    }

    public boolean remove(T data) {
        if (firstElement == null) { // lista jest pusta
            return false;
        } else {
            Node current = firstElement;
            Node prev = null;

            while (current != null) {
                if (current.value == data) { // znaleziono obiekt do usuniecia

                    if (prev != null) {
                        prev.nextNode = current.nextNode;
                    } else {
                        firstElement = firstElement.nextNode;
                    }
                }

                prev = current;
                current = current.nextNode;
            }
            return false;
        }
    }

    public boolean add(T data) {
        if (firstElement == null) {
            firstElement = new Node<>(data);
        } else {
            Node<T> someElement = new Node<T>(data);
            Node<T> lastNode = findLast();
            lastNode.nextNode = someElement;
        }

        System.out.println("Dodano element, rozmiar=" + size());
        return true;
    }

    public Node findLast() {
        Node<T> nextElement = firstElement.nextNode;
        Node<T> result = firstElement;

        while (nextElement != null) {
            result = nextElement;
            nextElement = nextElement.nextNode;
        }
        return result;
    }

    void showAll() {
        Node<T> current = firstElement;

        System.out.print("[");
        while (current != null) {
            System.out.print(current.value);
            if ()
                current = current.nextNode;
        }
        System.out.print("]");
    }

    public boolean isEmpty() {
        return firstElement == null;
    }

    public boolean removeAll() {
        if (firstElement == null){
            return false;
        }
        firstElement.nextNode = null;

    }
}