package com.sda.organizer.controller;

import com.sda.organizer.domain.User;
import com.sda.organizer.repository.UserRepository;
import com.sda.organizer.service.UserService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.security.MessageDigest;

/**
 * Podstawowy kontroller dla widoku JavyFx.
 */
@Component
public class LoginController extends BaseController {

    @FXML
    public ChoiceBox langChoice;

    @Autowired
    UserRepository userRepository;


    private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);
    public TextField login;
    public PasswordField password;

    @Autowired
    public UserService userService;

    public void onSelectLang(ActionEvent actionEvent) {
        Object lang = langChoice.getSelectionModel().getSelectedItem();
        if (lang.equals("english")) {
            springFXLoader.setCurrentLocaleLang("en");

        } else if (lang.equals("portugues")) {
            springFXLoader.setCurrentLocaleLang("pt");
        } else if (lang.equals("polski")) {
            springFXLoader.setCurrentLocaleLang("pl");
        }

        switchStage(langChoice, "/scenes/login/login.fxml");
    }


    public void onRegistry(ActionEvent actionEvent) {
        switchStage(langChoice, "/scenes/registry/registry.fxml");
    }

    public void onLogin(ActionEvent actionEvent) {


        User userFromDb = userRepository.findByUserName(login.getText());
        String hashedPassword = new String();

        byte [] bytesOfPassword = password.getText().getBytes();
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] thedigest = md.digest(bytesOfPassword);
            String temp = new String(thedigest);
            hashedPassword = temp;



        } catch (Exception e) {
            e.printStackTrace();
        }

        if (userFromDb.getUserPassword().equals(hashedPassword)) {
            switchStage(langChoice, "/scenes/add/add.fxml");
        } else {
            LOGGER.info("Zle");
        }

        userService.setCurrentUser(userFromDb);

    }
}

