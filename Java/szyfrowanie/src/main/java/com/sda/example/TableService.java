package com.sda.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Array;
import java.util.function.Predicate;

/**
 * Created by RENT on 2016-09-15.
 */
public class TableService {
    private final static int BLOCK_SIZE = 8;
    private final static String EMPTY = " ";

    private static final Logger LOGGER = LoggerFactory.getLogger(TableService.class);

    public <T> void show(final T[][] input) {
        for (int i = 0; i < input.length; i++) {
            T[] temp = input[i];
            for (int j = 0; j < temp.length; j++) {
                System.out.print(temp[j] + " | ");
            }
            System.out.print("\n");
        }
    }

    public void showString(final String[][] input) {
        for (int i = 0; i < input.length; i++) {
            String[] tempArray = input[i];
            for (int j = 0; j < tempArray.length; j++) {
                System.out.print(tempArray[j] + " | ");
            }
            System.out.print("\n");
        }
    }

    /**
     * PrediCat dostępny od Java 8. Pozwala zatąpić cześć ifa danym kodem
     */
    private final Predicate<Integer[][]> isCorrectArray = t -> t.length > 0 && t[0].length == t.length;

    public void chcekArraySize(Integer[][] input1, Integer[][] input2) {
        if (!isCorrectArray.test(input1) || !isCorrectArray.test(input2) || input1.length == input2.length) {
            throw new IllegalArgumentException("Rozna wielkosc tablic");
        }
    }

    public void chcekArraySize2(Integer[][] input1, Integer[][] input2) {
        if (input1.length != 0 && input1.length == input2.length && input1[0].length == input2[0].length) {
            return;
        } else {
            throw new IllegalArgumentException("Rozna wielkosc tablic");
        }
    }

    public void chcekArraySize3(Integer[][] input1, Integer[][] input2) {
        assert input1.length != 0 : "Maciez o zerowej wartosci";
        assert input1.length == input2.length : "tablice rozbych wielkosci";
        assert input1[0].length == input2[0].length : "tablice wewnetrzne roznych wielosci";
    }

    public Integer[][] add(Integer[][] input1, Integer[][] input2) {
        Integer[][] tempArray = new Integer[input1.length][input1[0].length];
        chcekArraySize3(input1, input2);
        for (int i = 0; i < input1.length; i++) {
            for (int j = 0; j < tempArray.length; j++) {
                tempArray[i][j] = input1[i][j] + input2[i][j];
            }
            System.out.print("\n");
        }
        return tempArray;
    }

    public Integer[][] sub(Integer[][] input1, Integer[][] input2) {
        Integer[][] tempArray = new Integer[input1.length][input1[0].length];
        chcekArraySize3(input1, input2);
        for (int i = 0; i < input1.length; i++) {
            for (int j = 0; j < tempArray.length; j++) {
                tempArray[i][j] = input1[i][j] - input2[i][j];
            }
            System.out.print("\n");
        }
        return tempArray;
    }

    public Integer[][] multiply(Integer[][] input1, Integer[][] input2) {
        Integer[][] tempArray = new Integer[input1.length][input1[0].length];
        chcekArraySize3(input1, input2);
        for (int i = 0; i < input1.length; i++) {
            for (int j = 0; j < tempArray.length; j++) {
                tempArray[i][j] = input1[i][j] * input2[i][j];
            }
            System.out.print("\n");
        }
        return tempArray;
    }

    public String fillToBlockSize(final String input) {
        LOGGER.info("Input data: {}", input.length());

        String result = input;
        int restElements;
        int amountElementsInBlock = BLOCK_SIZE * BLOCK_SIZE;

        if (input.length() > amountElementsInBlock) {
            restElements = amountElementsInBlock -
                    (input.length() % amountElementsInBlock);
        } else {
            restElements = amountElementsInBlock - input.length();
        }

        if (restElements == 0) {
            return result;
        }

        for (int i = 0; i < restElements; i++) {
            result += EMPTY;
        }
        LOGGER.info("Output data: {}", result.length());
        return result;
    }

    public String[][][] splitToBlock(final String input) {
        //wyjsciowa tablica trojwymiarowa
        String[][][] result =
                new String[input.length() / (BLOCK_SIZE * BLOCK_SIZE)][BLOCK_SIZE][BLOCK_SIZE];
        int currentPositionInInputString = 0;

        // Petla po blokach tekstu
        for (int i = 0; i < input.length() / (BLOCK_SIZE * BLOCK_SIZE); ++i) {
            // petla po kolumnach pojedynczego bloku
            for (int k = 0; k < BLOCK_SIZE; ++k) {
                // petla po wierszach pojedynczego bloku
                for (int w = 0; w < BLOCK_SIZE; ++w) {
                    if (currentPositionInInputString == input.length()) {
                        result[i][k][w] = input.substring(currentPositionInInputString);
                    } else {
                        result[i][k][w] = input.substring(currentPositionInInputString,
                                currentPositionInInputString + 1);
                    }

                    currentPositionInInputString++;
                }
            }//koniec petli po kolumnach pojedynczego bloku
        }// koniec petli po blokach

        return result;
    }

    public Integer[][][] convertToAsci(String[][][] stringsBlock) {
        Integer[][][] result = new Integer[stringsBlock.length][stringsBlock[0].length][stringsBlock[0].length];
        // Petla po blokach tekstu
        for (int i = 0; i < stringsBlock.length; ++i) {
            // petla po kolumnach pojedynczego bloku
            result[i] = convertToAsci(stringsBlock[i]);
        }// koniec petli po blokach

        return result;
    }

    public Integer[][] convertToAsci(String[][] strings) {
        Integer[][] integers = new Integer[BLOCK_SIZE][BLOCK_SIZE];
        for (int k = 0; k < BLOCK_SIZE; ++k) {
            // petla po wierszach pojedynczego bloku
            for (int w = 0; w < BLOCK_SIZE; ++w) {
                integers[k][w] = (int) strings[k][w].charAt(0);
            }
        }
        return integers;
    }

    public String[][] toSingleBlock(final String input) {
        assert input.length() > 0 : "Hasło nie moze byc puste";
        int currentPositionInInputString = 0;
        String[][] result = new String[BLOCK_SIZE][BLOCK_SIZE];
        for (int i = 0; i < BLOCK_SIZE; ++i) {
            for (int j = 0; j < BLOCK_SIZE; ++j) {
                if (currentPositionInInputString >= input.length()) {
                    currentPositionInInputString = 0;
                }
                result[i][j] = String.valueOf(input.charAt(currentPositionInInputString));
                currentPositionInInputString++;
            }
        }
        return result;
    }

    public Integer[][] shift(Integer[][] input) {
        Integer[][] shifted = new Integer[BLOCK_SIZE][BLOCK_SIZE];
        for (int i = 0; i < BLOCK_SIZE; ++i) {
            for (int j = 0; j < BLOCK_SIZE; ++j) {
                shifted[j][i] = input[i][j];
            }
        }
        return shifted;
    }

    public String convertFromBlock(Integer[][] input) {
        String result = "";
        for (int i = 0; i < BLOCK_SIZE; ++i) {
            for (int k = 0; k < BLOCK_SIZE; ++k) {
                result += String.valueOf((char) (int) input[i][k]);
            }
        }

        return result;
    }
}